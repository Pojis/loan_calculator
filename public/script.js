var language = 'zh';//默认是中文
var textMaps = [{
    'zh': '每月还款金额: ',
    'en': 'Monthly Payment: '
},
{
    'zh': '首月还款金额: ',
    'en': 'First Month Payment: '
}
];
var textMapsSimpleRate = {
    'zh': '年化单利: ',
    'en': 'Annual Interest Rate: '
};
OutofRangeIndex = -1;
var textMapsOutofRange = [
    {
        'zh': '低于0%',
        'en':'Below 0%'
    },
    {
        'zh': '高于100%，远离高利贷',
        'en':'Above 100%'
    }
]
// var textMapsCompoundRate = {
//     'zh': '年化复利: ',
//     'en': 'Annual Compound Interest Rate: '
// };
var time_unit//12为年，1为月
var result_text
var result_number
var monthlyPayment = 0;
var payment_method = 0;//等额本息(0)还是等额本金(1)
var cal_option = 0;//计算利率(1)还是计算月供(0)
var alert_msg={
    'zh': '请输入所有三个部分，不要有负数。',
    'en':'Please fill all of the three blanks, negative number is invalid.'
}
var contents = {
    en: {
        cal_payment_btn: 'Get Payment',
        cal_interest_btn: 'Get Interest',
        loanAmount_text: 'Loan Amount',
        loanAmount: 'For Example: 1000000',
        annualInterestRate_text: 'Annual Interest Rate',
        annualInterestRate: 'For Example: 5',
        monthlyPayment_text: 'Monthly Payment',
        monthlyPayment: 'For Example: 5000',
        loanTerm_text: 'Loan Term',
        loanTerm: 'For Example: 30',
        selectOptionYear: 'Year',
        selectOptionMonth:'Month',
        calculate_button: 'Calculate',
        equal_principal: 'Even Principal Payments',
        equal_payments: 'Even Total Payments'

        // ... 其他英文内容  
    },
    zh: {
        cal_payment_btn: '求月供',
        cal_interest_btn: '求利率',
        loanAmount_text: '贷款总额:',
        loanAmount: '例如：1000000',
        annualInterestRate_text: '年利率 (%):',
        annualInterestRate: '例如：5',
        monthlyPayment_text: '每期还款额',
        monthlyPayment: '例如：5000',
        loanTerm_text: '贷款期限:',
        loanTerm: '例如：30',
        selectOptionYear: '年',
        selectOptionMonth: '月',
        calculate_button: '计算',
        equal_principal: '等额本金',
        equal_payments: '等额本息'
        // ... 其他中文内容  
    }
    // ... 可以添加更多语言的内容  
};


function calculate_monthly_payment_etp(loanAmount, annualInterestRate, loanTerm) {//等额本息的每月还款额计算
    monthlyPayment = loanAmount * annualInterestRate / (1 - Math.pow((1 + annualInterestRate), -loanTerm));
    return monthlyPayment;
}
function calculate() {
    // 验证输入值  
    OutofRangeIndex = -1;

    var time_unit_seletor = document.getElementById('time-unit-select-box');
    time_unit = time_unit_seletor.value;
    var loanAmount = parseFloat(document.getElementById('loanAmount').value);
    var monthlyPayment = parseFloat(document.getElementById('monthlyPayment').value);
    var loanTerm = parseInt(document.getElementById('loanTerm').value) * time_unit; // 转换为总月份  
    var annualInterestRate = parseFloat(document.getElementById('annualInterestRate').value) / 100 / 12; // 转换为月利率  

    if (isNaN(loanAmount) || isNaN(loanTerm) || loanAmount <= 0  || loanTerm <= 0) {
        alert(alert_msg[language]);
        return false;
    }
    var selectElement = document.getElementById('payment_method');
    payment_method = selectElement.selectedIndex;
    if (cal_option) {
        // 求利率
        if (isNaN(monthlyPayment) || monthlyPayment <= 0) {
            alert(alert_msg[language]);
            return false;
        }
        flag = calculateInterest(loanAmount, monthlyPayment, loanTerm);
    } else {
        // 求月供
        if (isNaN(annualInterestRate) || annualInterestRate <= 0) {
            alert(alert_msg[language]);
            return false;
        }
        flag = calculateMortgage(loanAmount, annualInterestRate, loanTerm);
    }
    if (flag) {
        document.getElementById('result').style.display = 'block'
    }
}
function calculateInterest(loanAmount,monthlyPayment,loanTerm) {

    // 下面根据两种模式分别计算利率
    var monthlyRate
    var annualSimpleRate //年化单利
    var annualCompoundRate
    if (payment_method) {//等额本金，
        let montnlyPrincipal = loanAmount / loanTerm;
        monthlyRate = (monthlyPayment - montnlyPrincipal) / loanAmount;
        annualCompoundRate=Math.pow(1+monthlyRate,12)-1
    } else {//等额本息的利率
        // 先处理几种特殊情况
        // 使用二分查找有点太瞧不起当代计算机了，这里我们直接BF，总共也就那么几万个数而已。用二分既浪费程序员大脑，又容易出bug。
        for (i = 0.0001; i < 1.613; i += 0.0001){
            // 注意这里的i是复利值，单利的100%对应复利的161%，所以我们多算一点。
            // 为了精确，我们的年利率穷举小数点后2位（0.01%），最高到100%
            if (monthlyPayment * loanTerm < loanAmount) {
                OutofRangeIndex = 0;
            } else if (calPrincipal(1.613, monthlyPayment, loanTerm) > loanAmount) {
                OutofRangeIndex = 1;
            } else {
                OutofRangeIndex = -1;
                this_principal = calPrincipal(i, monthlyPayment, loanTerm);//根据当前的irr算出来的本金
                if (this_principal < loanAmount) {//本金少了，说明利率高了（可以想一想，因为还款额、期限是定的，是不是本金越少，相同的还款额下利率越高呢）
                    // 这样的话说明现在这个i和刚才那个有一个是对的，比较一下就可以了。
                    last_principal = calPrincipal(i - 0.0001, monthlyPayment, loanTerm);
                    if (last_principal - loanAmount > loanAmount - this_principal) {//这一次更精确
                        annualCompoundRate = i;
                    } else {
                        annualCompoundRate = i - 0.0001;
                    }
                    break;
                }
            }
        }
        monthlyRate = Math.pow(annualCompoundRate + 1, 1 / 12) - 1;
        

    }    
    annualSimpleRate = monthlyRate * 12

    if (OutofRangeIndex == -1) {
        result_number = (annualSimpleRate * 100).toFixed(2) + '%';
    }
    updateResult();
    return true;
}
// 已知irr求本金
function calPrincipal(irr, monthlyPay, month) {
    let ret = 0;
    for (let i = 1; i <= month; i++) {
        // 在JavaScript中，Math.pow(base, exponent) 用于计算底数（base）的指数次幂（exponent）  
        ret += monthlyPay / Math.pow((1 + irr), i/12 );
    }
    // 如果需要返回整数，可以使用 Math.floor, Math.ceil 或 Math.round  
    // 但由于贷款本金通常是浮点数，这里直接返回浮点数可能更合适  
    return ret;
}
function calculateMortgage(loanAmount, annualInterestRate, loanTerm) {
    monthlyPayment = 0; 

    // 分两种情况计算每月还款金额  
    if (payment_method == 0) {//等额本息
        monthlyPayment = calculate_monthly_payment_etp(loanAmount, annualInterestRate, loanTerm);
    } else {
        monthlyPayment = loanAmount / loanTerm + loanAmount * annualInterestRate;
    }


    // 显示结果  

    result_number = monthlyPayment.toFixed(2);
    updateResult();
    return true;
}
function updateResult() {

    // 结果的更新，不需要接收参数，因为全部都是全局变量
    if (cal_option) {//求利率
        if (OutofRangeIndex >= 0) {
            result_number = textMapsOutofRange[OutofRangeIndex][language];
        }
        result_text = textMapsSimpleRate[language];

    } else {//求月供

        result_text = textMaps[payment_method][language];

    }
    document.getElementById('result').textContent = result_text + result_number;
}
function switchLanguage(newLanguage) {
    language = newLanguage;
    updateResult();
    updateContent();
}
function change_cal_option_layout(mode) {//0表示计算月供，1表示计算利率
    cal_option = mode;

    //先改变两个按钮的颜色以展示选中状态。
    if (cal_option) {//为1表示求利率
        document.getElementById('cal_interest_btn').classList.add('selected_btn');
            document.getElementById('cal_payment_btn').classList.remove('selected_btn');
    }
    else {//求月供模式
        document.getElementById('cal_payment_btn').classList.add('selected_btn');
        document.getElementById('cal_interest_btn').classList.remove('selected_btn');
    }
    // 接下来改变布局，让中间那一行的输入框变成对应模式的参数  
    if (cal_option) {
        document.getElementById('InterestRate').style.display = 'none';
        document.getElementById('MonthlyPayment').style.display = 'block';
    } else {
        document.getElementById('InterestRate').style.display = 'block';
        document.getElementById('MonthlyPayment').style.display = 'none';
    }
    document.getElementById('result').style.display = 'none'


}
// 更新页面内容函数  
function updateContent() {
    var elements = document.getElementsByTagName('*'); // 获取所有元素  
    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        var id = element.id; // 获取元素的ID  
        var tagName = element.nodeName; // 例如: "DIV", "INPUT", "SPAN" 等

        // 检查当前元素是否有对应的中英文内容  
        if (contents[language].hasOwnProperty(id)) {
            if (tagName == 'INPUT') {
                element.placeholder = contents[language][id];
            }
            element.textContent = contents[language][id]; // 更新文本内容  
        }
    }
}

change_cal_option_layout(0)
updateContent();
